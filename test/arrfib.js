const chai = require('chai');
const { expect } = chai;
const requireNew = require('require-new');

describe('Get an element', () => {
    let arrFib;
    
    beforeEach( () => {
        arrFib = requireNew('../src/arrfib');
    })

    it('with Number index', () => {
        expect(arrFib[5]).equal(5n);
    });
    
    it('with String index', () => {
        expect(arrFib['5']).equal(5n);
    });
    
    it('with BigInt index', () => {
        expect(arrFib[5n]).equal(5n);
    });

});

describe('Check is number a part of Fibonacci sequence', () => {
    let arrFib;
    
    beforeEach( () => {
        arrFib = requireNew('../src/arrfib');
    });

    it('existing BigInt with "in"', () => {
        expect(5n in arrFib).equal(true);
    });

    it('existing Number with "in"', () => {
        expect(13 in arrFib).equal(true);
    });

    it('non existing BigInt with "in"', () => {
        expect(42n in arrFib).equal(false);
    });

    it('non existing Number with "in"', () => {
        expect(99 in arrFib).equal(false);
    });

    it('non existing String with "in"', () => {
        expect('something' in arrFib).equal(false);
    });

    it('existing BigInt with "includes"', () => {
        expect(arrFib.includes(5n)).equal(true);
    });

    it('existing Number with "includes"', () => {
        expect(arrFib.includes(13)).equal(true);
    });

    it('non existing BigInt with "includes"', () => {
        expect(arrFib.includes(42n)).equal(false);
    });

    it('non existing Number with "includes"', () => {
        expect(arrFib.includes(99)).equal(false);
    });

    it('non existing String with "includes"', () => {
        expect(arrFib.includes('something')).equal(false);
    });

});

describe('Get index in Fibonacci sequence', () => {
    let arrFib;
    
    beforeEach( () => {
        arrFib = requireNew('../src/arrfib');
    });

    it('existing Number', () => {
        expect(arrFib.indexOf(5)).equal(5);
    });

    it('non existing Number', () => {
        expect(arrFib.indexOf(6)).equal(-1);
    });

    it('existing BigInt', () => {
        expect(arrFib.indexOf(5n)).equal(5);
    });

    it('non existing BigInt', () => {
        expect(arrFib.indexOf(6n)).equal(-1);
    });

    it('existing String number', () => {
        expect(arrFib.indexOf('5')).equal(5);
    });
    
    it('non existing String number', () => {
        expect(arrFib.indexOf('6')).equal(-1);
    });

});

describe('Get part of Fibonacci sequence', () => {
    let arrFib;
    
    beforeEach( () => {
        arrFib = requireNew('../src/arrfib');
    });

    it('slice first 10 numbers', () => {
        expect(arrFib.slice(0, 10)).to.eql([0n, 1n, 1n, 2n, 3n, 5n, 8n, 13n, 21n, 34n]);
    });

    it('slice five numbers not from start', () => {
        expect(arrFib.slice(20, 25)).to.eql([6765n, 10946n, 17711n, 28657n, 46368n]);
    });

    it('invalid slice starts with negative number', () => {
        expect(arrFib.slice(-1, 5)).to.eql([]);
    });

    it('invalid slice with negative numbers, start < end', () => {
        expect(arrFib.slice(-8, -5)).to.eql([]);
    });

    it('invalid slice with negative numbers, start > end', () => {
        expect(arrFib.slice(-5, -8)).to.eql([]);
    });

    it('invalid slice with negative numbers, start = end', () => {
        expect(arrFib.slice(-9, -9)).to.eql([]);
    });

    it('invalid slice with positive numbers', () => {
        expect(arrFib.slice(50, 49)).to.eql([]);
    });
});

describe('Using in for .. of loop', () => {
    let arrFib;
    
    beforeEach( () => {
        arrFib = requireNew('../src/arrfib');
    });

    it('get first 6 elements', () => {
        const seq = [];
        for (const n of arrFib){
            seq.push(n);
            if (n >= 5) break;
        }
        expect(seq).to.eql([0n, 1n, 1n, 2n, 3n, 5n]);
    });
});

describe('Using forEach', () => {
    let arrFib;
    
    beforeEach( () => {
        arrFib = requireNew('../src/arrfib');
    });

    it('get first 7 elements', () => {
        const seq = {};

        try {
            arrFib.forEach((v, i) => {
                seq[i] = v;
                if(i > 5) throw 'Escape from infinity loop';
            })
        } catch (error) {

        }

        expect(seq).to.eql({0: 0n, 1: 1n, 2: 1n, 3: 2n, 4: 3n, 5: 5n, 6: 8n});
    });
});