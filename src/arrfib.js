((root, factory)=>{
    if (typeof define === 'function' && define.amd) {
        define(factory);
    } else if (typeof exports === 'object') {
        module.exports = factory();
    } else {
        root.arrFib = factory();
    }
})(this, () => {
    const FIB_SEQ_LIMIT = 5e5;

    let collection = {
        0: 0n, 
        1: 1n, 
        2: 1n,
        length: 3,
        [Symbol.iterator]: function* (){
            let n1 = 0n;
            let n2 = 1n;
            for(let nC = n1; nC < Infinity; nC = n1){
                yield nC;
                n1 = n2;
                n2 = n1 + nC;
            }
        }
    };
    let handlers = {
        set(){
            throw "Fibonacci sequence is read-only!";
        }, 
        get(target, propName) {
            
            if (propName === Symbol.iterator){
                return target[Symbol.iterator];
            }

            if (propName === 'includes'){
                return (number) => this.has(target, number);
            }
            
            if (propName === 'indexOf'){
                return (number, fromIndex) =>{
                    if(number === Number.POSITIVE_INFINITY) return fromIndex ? fromIndex + FIB_SEQ_LIMIT : FIB_SEQ_LIMIT;
                    return (this.has(target, number) && [].indexOf.call(target, BigInt(number), fromIndex)) || -1;
                }  
            }
            
            if (propName === 'lastIndexOf'){
                return (number, fromIndex) =>{
                    if(number === Number.POSITIVE_INFINITY) return Number.POSITIVE_INFINITY;
                    return (this.has(target, number) && [].lastIndexOf.call(target, BigInt(number), fromIndex)) || -1;
                } 
            }
            
            if (propName === 'find'){
                return () => { throw `You found an easter egg!` };
            }

            if (propName === 'forEach'){
                return (clb, thisArg = target) => {
                    let iterator = 0;
                    let tArr = [];
                    for (let n of target){
                        clb.call(thisArg, n, iterator++, tArr);
                    }
                }
            }
            
            if (propName === 'slice'){
                return (start, end) => ((start < end) && this.get(target, end)) ? [].slice.call(target, start, end) : [];
            }
            
            if (propName === 'length'){
                return Number.MAX_SAFE_INTEGER;
            }
            
            if (propName === Number.POSITIVE_INFINITY){
                return Number.POSITIVE_INFINITY;
            }
            
            if (propName > FIB_SEQ_LIMIT){
                return Number.POSITIVE_INFINITY;
            }
            
            let n = parseInt(propName);
            if ( typeof n !== 'number' || isNaN(n) || n != propName ) return undefined;
            
            while (target.length <= n){
                target[target.length] = target[target.length - 1] + target[target.length - 2]; 
                target.length++;
            }
            
            return target[n];
        },
        has(target, propName){

            let propType = typeof propName;

            if (propType === 'string' && (propName === '0' || +propName > 0)){
                propName = +propName;
                propType = typeof propName;
            }

            if (!['bigint', 'number'].includes(propType)) return false;

            if (propName === Number.POSITIVE_INFINITY) return true;

            let numberToCheck = propName;
            if (propType !== 'bigint'){
                numberToCheck = parseInt(propName);
                if ( numberToCheck < 0 || (numberToCheck === 0 && propName !== 0) || isNaN(numberToCheck) ) return false;
                numberToCheck = BigInt(propName);
            }

            while(target[target.length - 1] < numberToCheck){
                target[target.length] = target[target.length - 1] + target[target.length - 2];
                target.length++;
            }

            return [].includes.call(target, numberToCheck);
        }
    };

    return new Proxy(collection, handlers);
});
