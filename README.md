# arrFib.js
_[ 0n, 1n, 1n, 2n, 3n, 5n, 8n ... 267914296n, 433494437n ... Infinity ]_
> Fibonacci sequence in [BigInt](https://javascript.info/bigint) format with array-like interface:
* `arrFib[n]`
* `n in arrFib`
* `arrFib.includes(n)`
* `arrFib.indexOf(n)`
* `arrFib.slice(start, end)`
* `arrFib.forEach(clb)`

## Changes
### __v1.1__ (05/19/2020)
* Fixed bug - checking is number a part of sequence with `in`
* Fixed bug - slice from start returns an empty array
* Checking "string Numbers" (`'25', '125'` etc.) works like normal Numbers.
```js
'610' in arrFib // true
arrFib.includes('610') // true
arrFib.indexOf('610') // 15
// Will not work with BigInt strings
'610n' in arrFib // false
arrFib.includes('610n') // false
arrFib.indexOf('610n') // -1
``` 

## Get an element
Simple as getting array element;
```js
console.log(arrFib[0]); // 0n
console.log(arrFib[3]); // 2n
console.log(arrFib[3n]); // 2n
console.log(arrFib['3']); // 2n
console.log(arrFib[45]); // 1134903170n
console.log(arrFib[101]); // 573147844013817084101n
console.log(arrFib[3e2]); // 222232244629420445529739893461909967206666939096499764990979600n
console.log(arrFib[499999]); // Out-of-memory crash or "Huge number"
console.log(arrFib[500000]); // Infinity
console.log(arrFib.length); // Infinity
```
__Attantion!__ Geting big numbers can took a long time or even crash the browser.


## Check is number in Fibonacci sequence

```js
console.log(610n in arrFib); // true
console.log(arrFib.includes(610n)); // true
console.log(42n in arrFib); // false
console.log(arrFib.includes(42n)); // false

// Checking numbers in non BigInt format is allowed too
console.log(42 in arrFib); // false
console.log(arrFib.includes(42)); // false
```


## Get the position of number in Fibonacci sequence
`arrFib.indexOf` work's the same as [Array.prototype.indexOf](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/IndexOf)
```js
console.log(arrFib.indexOf(42)); // -1

console.log(arrFib.indexOf(610n)); // 15
console.log(arrFib.indexOf(610)); // 15
console.log(arrFib.indexOf('610')); // 15
```
`arrFib.lastIndexOf` also work's the same as [Array.prototype.lastIndexOf](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/lastIndexOf)


## Get a part of Fibonacci sequence
`arrFib.slice` work's the same as 

```js
let a1 = arrFib.slice(1, 10); // (9) [1n, 1n, 2n, 3n, 5n, 8n, 13n, 21n, 34n]
let a2 = arrFib.slice(100, 150); // Array with 50 elements

let ae0 = arrFib.slice(100, 50); // Empty array
```

## Using in loops
Atantion! All loops are infinity! So, sooner or later it wil couse an browser crash.

`arrFib.forEach` work's the same as [Array.prototype.forEach](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach). 
```js
    arrFib.forEach((number, index)=>{
        console.log(`arrFib[${index}] = ${number}`);
        
        if (index > 1000) throw 'Escape from infinity loop';
    });
```

You also can use `for .. of` same as arrays
```js
async function fibList(){
    for (let n of arrFib){
        console.log(`${n}`);
        await new Promise(r => setTimeout(r, 500));
        if(n > 1e10) break;
    }
    console.log('done!');
}
fibList();
```

# Don't take it seriously!
> It's just a joke about answers to interview questions.

But. It work's! If anyone use it as a solution - please [let me know](mailto:pratnomenis+fib@gmail.com).